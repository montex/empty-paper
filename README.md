**Resulting Document**
* [PDF](https://gitlab.com/montex/empty-paper/-/jobs/artifacts/master/file/main.pdf?job=pdf)

**Track Changes**
* [PDF](https://gitlab.com/montex/empty-paper/-/jobs/artifacts/master/file/main_diff.pdf?job=diff-baseline) (baseline) 
* [PDF](https://gitlab.com/montex/empty-paper/-/jobs/artifacts/master/file/main_diff_previous.pdf?job=diff-previous) (previous)  

The "baseline" version is by default the _previous tag_ present in the repository.
